var gulp = require('gulp'),
	pug = require('gulp-pug'),
	cleancss = require('gulp-clean-css'),
	rename = require('gulp-rename'),
	autoprefixer = require('gulp-autoprefixer'),
	notify = require("gulp-notify"),
	sass = require('gulp-sass')
	uglify = require('gulp-uglify')
	browserSync = require('browser-sync');

gulp.task('browser-sync', function() {
	browserSync({
		server: {
			baseDir: 'src'
		},
		notify: false
	})
});

gulp.task('pug', function() {
	return gulp.src('src/pug/pages/**/*.pug')
		.pipe(pug({
			pretty: true
		}))
		.pipe(gulp.dest('src'))
		.on('end', browserSync.reload);
});

gulp.task('sass', function() {
	return gulp.src('src/sass/**/*.sass')
		.pipe(sass({ outputStyle: 'expanded' }).on("error", notify.onError()))
		.pipe(autoprefixer(['last 15 versions']))
		.pipe(gulp.dest('src/css'))
		.pipe(rename({ suffix: '.min', prefix : '' }))
		.pipe(cleancss( {level: { 1: { specialComments: 0 } } }))
		.pipe(gulp.dest('src/css'))
		.pipe(browserSync.reload({
			stream: true
		}));
});

gulp.task('js', function() {
	return gulp.src(['src/js/main.js', 'src/js/index.js'])
		.pipe(uglify())
		.pipe(rename({ suffix: '.min', prefix : '' }))
		.pipe(gulp.dest('src/js'))
		.pipe(browserSync.reload({
			stream: true
		}));
});

gulp.task('watch', function() {
	gulp.watch('src/pug/**/*.pug', gulp.series('pug'));
	gulp.watch('src/sass/**/*.sass', gulp.series('sass'));
	gulp.watch(['src/js/main.js', 'src/js/index.js'], gulp.series('js'));
});

gulp.task('default', gulp.series(
	gulp.parallel('pug', 'sass', 'js'),
	gulp.parallel('watch', 'browser-sync')
));